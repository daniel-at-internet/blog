<!--
.. title: "Configuración de Nikola"
.. slug: configuracion-de-nikola
.. date: 2021-07-24 21:07:48 UTC+02:00
.. tags: nikola, config
.. category: 
.. link: 
.. description: 
.. type: text
.. author: DanielSan
-->

# Instalación

Simplemente seguir las instrucciones del
[getting started](https://getnikola.com/getting-started.html).

``` sh
cd ~/git/blog
python3 -m venv nikola_venv
source ~/git/blog/nikola_venv/bin/activate
~/git/blog/nikola_venv/bin/python -m pip install -U pip setuptools wheel
~/git/blog/nikola_venv/bin/python -m pip install -U "Nikola[extras]"
```

# Crear un sitio de demostración

``` sh
nikola init --demo demo
# Respondo a unas cuantas preguntas que se usan como variables en `config.py`:
#
# This is the main URL for your site. It will be used
# in a prominent link. Don't forget the protocol (http/https)!
# BLOG_AUTHOR = "DanielSan"
# BLOG_TITLE = "Algo que he visto"
# SITE_URL = "http://127.0.0.1:8000/"
# BLOG_EMAIL = ""
# BLOG_DESCRIPTION = "Temas que he visto y me gustaría tener a mando en 
#   cualquier ordenador. Si te son de utilidad... es un beneficio añadido."
cd demo
nikola build
nikola serve --browser
# Para ver los cambios al momento: Cortamos el serve (Ctrl+C) y...
nikola auto
```

# Configurar el "theme"

He visto que en [getnikola.com](https://getnikola.com/) tiene el theme
[cerulean](https://bootswatch.com/cerulean/) y decidí usar
[solar](https://bootswatch.com/solar/) (disponible en el mismo sitio).

Preguntando en libera.chat(irc)[#nikola](https://web.libera.chat/) como hacerlo,
me [dieron la solución](https://irclogs.getnikola.com/logs/2021-07-24/)
rápidamente.

``` sh
nikola subtheme -n solar_subtheme -s solar -p bootstrap4
cd demo  # Subdirectorio del sitio
vim config.py
# THEME = "solar_subtheme"
# CODE_COLOR_SCHEME = 'monokai'
```

# Generar un nuevo post

``` sh
nikola new_post --format=markdown --author=DanielSan --title="Configuración de Nikola"
```

# Publicar en GitLab

* Hice un fork de https://gitlab.com/pages/nikola
* Borré los directorios de ejemplo
``` sh
rm -fr files/ galleries/ images listings/ posts/ stories/ templates/
```
* Reemplacé el contenido del repositorio por el que tenía creado:
    * `config.py` → La configuración de Nikola
    * `themes/` → El tema
    * `posts/` → Los artículos
* Cambié la rama principal a main: 
    * gitlab.com → Settings → Repository → Default branch
* Actualicé el `README.md`
* Configuré el `OUTPUT_FOLDER`
``` sh
vim config.py
# OUTPUT_FOLDER = 'public'
```
* Activé el https: 
    * gitlab.com → Settings → Pages 
      → ✅ Force HTTPS (requires valid certificates) 
      → 🔳 Save changes
